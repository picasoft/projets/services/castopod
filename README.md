# Castopod

Castopod est un service de podcast en ligne disponible à l'adresse [https://podcast.picasoft.net](https://podcast.picasoft.net). Le service est découpé en 3 conteneurs, un service de base de données MySQL, un conteneur qui s'occupe des traitements PHP (`back`) et un conteneur qui sert le contenu statique (`front`).

## Mettre à jour

Il suffit de mettre à jour le numéro de version dans le fichier `docker-compose.yml` et dans les `Dockerfile` du `front` **et** du `back`. Puis on lance un `docker-compose build`.

**Attention** : Le logiciel est encore en *beta* les migrations des bases de données ne sont pas encore faites automatiquement, il faut vérifier les notes de release et les faire le cas échéant.

On pourra par exemple remplir un fichier SQL contenant le script de migration, le copier dans le conteneur (`docker cp`) puis ouvrir une console MariaDB dans le conteneur pour exécuter `source <chemin vers mon script>;`.

## Installation

Remplir les fichiers situés dans `secrets` et lancer le tout. Se rendre vers `/cp-auth` pour finir l'installation et créer le compte administrateur. Ensuite pour se connecter il faut se rendre vers `/cp-auth/login`, l'interface d'administration est disponible sur `/cp-admin`.

Il faut aussi penser à définir la variable `CP_BASEURL` obligatoire sur le back indiquant l'URL de base de l'instance, ici `https://podcast.picasoft.net`.
